const Course = require("../models/Course.js")

//Start of Activity

module.exports.addCourse = (reqBody) => {

	let newCourse = new Course({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	})

	return newCourse.save().then((course, error) => {

		if(error) {
			return false
		} else {
			return true
		}
	})

}

//End of Activity

//Retrieve all Courses

module.exports.getAllCourses = () => {

	return Course.find({}).then(result => {

		return result
	})
}

//Retrieve All Active Courses
module.exports.getAllActive = () => {

	return Course.find({isActive: true}).then(result =>  result);

}

//Retrieve a specific course
module.exports.getCourse = (reqParams) => {
	console.log(reqParams)

	return Course.findById(reqParams.courseId).then(result => result)
}

//Updating a course
module.exports.updateCourse = (data) => {
	console.log(data);

	return Course.findById(data.courseId).then((result, err) => {
		console.log(result);

		if(data.isAdmin) {
			result.name = data.updatedCourse.name
			result.description = data.updatedCourse.description
			result.price = data.updatedCourse.price

			console.log(result)

			return result.save().then((updatedCourse, error) => {

				if(error) {
					return false
				} else {
					return updatedCourse;
				}
			})

		} else {
			return "Not Admin"
		}
	})
}

// Start of Activity
//Controller for archiving a course

module.exports.archiveCourse = (data) => {

	return Course.findById(data.courseId).then((result, error) => {
		console.log(result);

		if(data.isAdmin) {

			result.isActive = false;

			return result.save().then((archivedCourse, error) => {

				if(error) {
					return false
				} else {
					return true
				}

			})

		} else {
			return "Authorized Personnels Only"
		}

	})

}

//End of Activity
